#!/bin/sh
#Written by : mahithm @ qikwell
#This script will launch a git tag/branch supplied as argument to all production servers mentioned in server.txt.
#This script is executed on the user system
#Prerequisities : pssh (sudo apt-get install pssh), server.txt with all server ip's on your home folder
#Usage : Argument 1 : tag/branch (mandatory) [./deployToProduction 1.0.6], Argument 2 : package names (optional) [./deployToProduction 1.0.6 "postgresql-devel"]  
#Get the required files. Specify the required file info here.
PEM_FILE=$HOME/.ssh/qikwell.pem
SERVER_FILE=$HOME/server.txt
if [ -z "$1" ]; then
	echo "No git tag/branch supplied!"
else
	#-h for hostnames
	#-l for username
	#-t for maximum time it should wait before it can timeout if request is not yet completed(in seconds). Currently set to 30 min as launching on all will never take up this much.
	#-x for passing arguments for pssh. Here we pass the ssh parameters like pem file
	#-tt for allowing sudo access on production servers
	#-oStrictHostKeyChecking=no for allowing access without host key authentication
	#-i for inline output from each server for the command executed
	parallel-ssh -h $SERVER_FILE -l ec2-user -t 1800 -x "-tt -oStrictHostKeyChecking=no  -i $PEM_FILE" -i "sh /ebs_store/deploy.sh $1 $2;"	
fi
