#!/bin/sh
#Written by : mahithm @ qikwell
#This script will launch a git tag/branch supplied as argument (./deploy.sh 1.0.6)
#This script is executed from the production server 
#This file needs to be on the production servers on the file path /ebs_store/ and the permission to be set as 777 to be on safer side. 
if [ -z "$1" ]; then
	echo "No git tag/branch supplied!"
else 
	echo "Setting the necessary environment path"
	PATH="/home/ec2-user/.rvm/gems/ruby-2.1.1/bin:/home/ec2-user/.rvm/gems/ruby-2.1.1@global/bin:/home/ec2-user/.rvm/rubies/ruby-2.1.1/bin:/home/ec2-user/.rvm/gems/ruby-2.1.1/bin:/home/ec2-user/.rvm/gems/ruby-2.1.1@global/bin:/home/ec2-user/.rvm/rubies/ruby-2.1.1/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/opt/aws/bin:/home/ec2-user/bin:/home/ec2-user/.rvm/bin"
	GEM_HOME='/home/ec2-user/.rvm/gems/ruby-2.1.1'
	GEM_PATH='/home/ec2-user/.rvm/gems/ruby-2.1.1:/home/ec2-user/.rvm/gems/ruby-2.1.1@global'
	MY_RUBY_HOME='/home/ec2-user/.rvm/rubies/ruby-2.1.1'
	IRBRC='/home/ec2-user/.rvm/rubies/ruby-2.1.1/.irbrc'
	echo "Navigating to the directory /ebs_store/launch/dhanvantri"
	cd /ebs_store/launch/dhanvantri
	echo "Removing local changed if any"
	git stash
	echo "Pulling out new branches"
	git pull
	echo "Checking out to the branch $1"
	git checkout $1
	if [ ! $? -eq 0 ]; then
		echo "Can't checkout to the given tag/branch!"
	else
		if [ ! -z "$2" ]; then
			echo "Executing yum install $2"
			sudo yum -y install $2
			if [ ! $? -eq 0 ]; then
				echo "Yum install failure!"
				exit 0
			fi
			echo "Yum install success!"
		fi
		echo "Removing the Gemfile.lock"
		sudo rm -rf Gemfile.lock
		echo "Bundle installing the gem files"
		bundle install
		echo "Removing the entries of public/assets/"
		sudo rm -rf public/assets/*
		echo "Assets precompiling starts!"
		rvmsudo rake tmp:clear
		rvmsudo rake assets:clean
		rvmsudo rake assets:precompile RAILS_ENV=production
		touch tmp/restart.txt
		echo "Assets precompiling done!"
		echo "Setting the required permissions for assets"
		sudo chmod 777 public/assets/*
		echo "Removing the apache logs starts"
		echo "Removing old access logs"
		sudo sh -c "rm -rf /etc/httpd/logs/access_log-*"
		echo "Removing old error logs"
		sudo sh -c "rm -rf /etc/httpd/logs/error_log-*"
		echo "Removing old deflate logs"
		sudo sh -c "rm -rf /etc/httpd/logs/deflate_log-*"
		echo "Removing the apache logs ends"
		echo "Restarting the apache server"
		sudo /etc/init.d/httpd restart
		echo "For cross checking the tag/branch deployed"
		git branch
	fi
fi
